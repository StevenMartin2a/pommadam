<?php
include '../../modele/db.php';
?>


<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Pommadan</title>
  </head>
  <body>

  <!-- Bar de navigation -->
  <div style="margin-top:40px;">
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Pommadan</a>
  <input class="form-control form-control-dark w-100" aria-label="Search" type="text" placeholder="Search">
  <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
      <a class="nav-link" href="#">Sign out</a>
    </li>
  </ul>
</nav>
</div>

<!-- Menu -->
<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link active" href="../../index.php">
              <svg xmlns="http://www.w3.org/2000/svg" class="feather feather-home" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" width="24" height="24"><path d="M 3 9 l 9 -7 l 9 7 v 11 a 2 2 0 0 1 -2 2 H 5 a 2 2 0 0 1 -2 -2 Z" /><polyline points="9,22 9,12 15,12 15,22" /></svg>
              Dashboard <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../reservation.php">
              <svg xmlns="http://www.w3.org/2000/svg" class="feather feather-shopping-cart" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" width="24" height="24"><circle cx="9" cy="21" r="1" /><circle cx="20" cy="21" r="1" /><path d="M 1 1 h 4 l 2.68 13.39 a 2 2 0 0 0 2 1.61 h 9.72 a 2 2 0 0 0 2 -1.61 L 23 6 H 6" /></svg>
              Réservations
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="../utilisateur.php">
              <svg xmlns="http://www.w3.org/2000/svg" class="feather feather-users" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" width="24" height="24"><path d="M 17 21 v -2 a 4 4 0 0 0 -4 -4 H 5 a 4 4 0 0 0 -4 4 v 2" /><circle cx="9" cy="7" r="4" /><path d="M 23 21 v -2 a 4 4 0 0 0 -3 -3.87" /><path d="M 16 3.13 a 4 4 0 0 1 0 7.75" /></svg>
              Utilisateurs
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <main class="col-md-9 ml-sm-auto col-lg-10 px-4" role="main"><div class="chartjs-size-monitor" style="left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; visibility: hidden; position: absolute; z-index: -1; pointer-events: none;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>

      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h2>Modifier un utilisateur</h2>
      </div>


<form action="#" method="post">

    <div class="form-group">
        <label for="nom">Nom</label>
        <?=input('nom') ?>
    </div>
    <div class="form-group">
        <label for="prenom">Prénom</label>
        <?= input('prenom') ?>
    </div>
    <div class="form-group">
        <label for="tel">Téléphone</label>
        <?=input('tel') ?>
    </div>
    <div class="form-group">
        <label for="mail">Mail</label>
        <?= input('mail') ?>
    </div>
    <button type="submit" class="btn btn-success">Enregistrer</button>
</form>