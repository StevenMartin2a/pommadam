<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Pommadan</title>
  </head>
  <body>

  <!-- Bar de navigation -->
  <div style="margin-top:40px;">
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">Pommadan</a>
  <input class="form-control form-control-dark w-100" aria-label="Search" type="text" placeholder="Search">
  <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
      <a class="nav-link" href="#">Sign out</a>
    </li>
  </ul>
</nav>
</div>

<!-- Menu -->
<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link active" href="index.php">
              <svg xmlns="http://www.w3.org/2000/svg" class="feather feather-home" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" width="24" height="24"><path d="M 3 9 l 9 -7 l 9 7 v 11 a 2 2 0 0 1 -2 2 H 5 a 2 2 0 0 1 -2 -2 Z" /><polyline points="9,22 9,12 15,12 15,22" /></svg>
              Dashboard <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="vue/reservation.php">
              <svg xmlns="http://www.w3.org/2000/svg" class="feather feather-shopping-cart" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" width="24" height="24"><circle cx="9" cy="21" r="1" /><circle cx="20" cy="21" r="1" /><path d="M 1 1 h 4 l 2.68 13.39 a 2 2 0 0 0 2 1.61 h 9.72 a 2 2 0 0 0 2 -1.61 L 23 6 H 6" /></svg>
              Réservations
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="vue/utilisateur.php">
              <svg xmlns="http://www.w3.org/2000/svg" class="feather feather-users" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" width="24" height="24"><path d="M 17 21 v -2 a 4 4 0 0 0 -4 -4 H 5 a 4 4 0 0 0 -4 4 v 2" /><circle cx="9" cy="7" r="4" /><path d="M 23 21 v -2 a 4 4 0 0 0 -3 -3.87" /><path d="M 16 3.13 a 4 4 0 0 1 0 7.75" /></svg>
              Utilisateurs
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <main class="col-md-9 ml-sm-auto col-lg-10 px-4" role="main"><div class="chartjs-size-monitor" style="left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; visibility: hidden; position: absolute; z-index: -1; pointer-events: none;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
      
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h2>Dashboard véhicules</h2>
      </div>
      <div class="table-responsive">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th>Id</th>
              <th>Modèle</th>
              <th>Place</th>
              <th>kilomètrage</th>
              <th>Etat</th>
              <th>Action</th>
            </tr>
            <?php foreach($vehicules as $vehicule): ?>
            <tr>
              <td><?= $vehicule['vehicule_id'] ?></td>
              <td><?= utf8_encode($vehicule['modele']) ?></td>
              <td><?= $vehicule['nb_place'] ?></td>
              <td><?= $vehicule['kilometrage'] ?></td>
              <td><?= utf8_encode($vehicule['etat']) ?></td>
              <td>
              <a  href="vue/vehicule/modification.php?vehiculeEdit=<?=$vehicule['vehicule_id'];?>" class="btn btn-outline-dark" >Modifier</a>
              <a  href="?vehiculeDelete=<?=$vehicule['vehicule_id'];?>" class="btn btn-outline-warning" onclick="return confirm('Voulez-vous suprimer ce véhicule?');">Supprimer</a>

              <?php if($vehicule['indisponible']==0){ ?>
                    <a href="?vehiculeDesactivation=<?=$vehicule['vehicule_id'];?>" class="btn btn-outline-danger">Indisponible</a> 
                <?php } else {?>
                    <a href="?vehiculeActivation=<?=$vehicule['vehicule_id'];?>" class="btn btn-outline-danger">Disponible</a>
                    <?php } ?>

              </td>
            </tr>
            <?php endforeach; ?>
          </thead>
        
        </table>
        <a href="vue/vehicule/ajout.php" class="btn btn-outline-info">Ajouter</a>

      </div>
    </main>

  </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"></script>
      <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="/docs/4.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
        <script src="dashboard.js"></script>

</body>
</html>