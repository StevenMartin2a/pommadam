<?php

try{
    $db = new PDO('mysql:host=localhost; dbname=pommadan', 'root', '');  
}catch(exception $e){
    echo 'impossible de se connecter à la base de données';
    die();
}


function input($id){
    $value=isset($_POST[$id]) ? $_POST[$id]:'';
    return "<input type='text' class='form-control' id=$id name='$id' value='$value'>";
}


// Affichage des véhicules
$select = $db->query('SELECT * FROM 2vehicule');
$vehicules = $select->fetchAll();

//Activation véhicule
if(isset($_GET['vehiculeActivation'])){
    $id = $db->quote($_GET['vehiculeActivation']);
    $db->query("UPDATE 2vehicule SET indisponible = '0'  WHERE vehicule_id=$id");
    header('Location:index.php');
    die();
}

//Désactivation véhicule
if(isset($_GET['vehiculeDesactivation'])){
    $id = $db->quote($_GET['vehiculeDesactivation']);
    $db->query("UPDATE 2vehicule SET indisponible = '1'  WHERE vehicule_id=$id");
    header('Location:index.php');
    die();
}

//Ajouter un véhicule

if (isset($_POST['modeleAdd']) && isset($_POST['nb_placeAdd']) && isset($_POST['kilometrageAdd']) && isset($_POST['etatAdd'])){
    $modele=$db->quote($_POST['modeleAdd']);
    $place=$db->quote($_POST['nb_placeAdd']);
    $kilometrage=$db->quote($_POST['kilometrageAdd']);
    $etat=$db->quote($_POST['etatAdd']);
    $db->query("INSERT INTO `2vehicule` SET modele=$modele, nb_place=$place, kilometrage=$kilometrage, etat=$etat ");
    header('Location:../../index.php');
    die();
}

//Modifier un véhicule
if (isset($_POST['modele']) && isset($_POST['nb_place']) && isset($_POST['kilometrage']) && isset($_POST['etat'])){
    $id = $db->quote($_GET['vehiculeEdit']);
    $modele=$db->quote($_POST['modele']);
    $place=$db->quote($_POST['nb_place']);
    $kilometrage=$db->quote($_POST['kilometrage']);
    $etat=$db->quote($_POST['etat']);
    $db->query("UPDATE `2vehicule` SET modele=$modele, nb_place=$place, kilometrage=$kilometrage, etat=$etat WHERE vehicule_id=$id ");
    header('Location:../../index.php');
    die();
}

// suppression véhicule
if(isset($_GET['vehiculeDelete'])){
    $id = $db->quote($_GET['vehiculeDelete']);
    $db->query("DELETE FROM 2vehicule WHERE vehicule_id=$id");
    header('Location:index.php');
    die();
}





//Affichage des utilisateurs
$use = $db-> query('SELECT * FROM 2user');
$users = $use->fetchAll();

//Activation utilisateur
if(isset($_GET['userActivation'])){
    $id = $db->quote($_GET['userActivation']);
    $db->query("UPDATE 2user SET indisponible = '0'  WHERE user_id=$id");
    header('Location:utilisateur.php');
    die();
}


//Désactivation utilisateur
if(isset($_GET['userDesactivation'])){
    $id = $db->quote($_GET['userDesactivation']);
    $db->query("UPDATE 2user SET indisponible = '1'  WHERE user_id=$id");
    header('Location:utilisateur.php');
    die();
}


//Modifier un utilisateur
if (isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['tel']) && isset($_POST['mail'])){
    $id = $db->quote($_GET['userEdit']);
    $nom=$db->quote($_POST['nom']);
    $prenom=$db->quote($_POST['prenom']);
    $tel=$db->quote($_POST['tel']);
    $mail=$db->quote($_POST['mail']);
    $db->query("UPDATE `2user` SET nom=$nom, prenom=$prenom, tel=$tel, mail=$mail WHERE user_id=$id ");
    header('Location:../utilisateur.php');
    die();
}

//Ajouter un utilisateur
if (isset($_POST['nomAdd']) && isset($_POST['prenomAdd']) && isset($_POST['telAdd']) && isset($_POST['mailAdd'])){
    $nom=$db->quote($_POST['nomAdd']);
    $prenom=$db->quote($_POST['prenomAdd']);
    $tel=$db->quote($_POST['telAdd']);
    $mail=$db->quote($_POST['mailAdd']);
    $db->query("INSERT INTO `2user` SET nom=$nom, prenom=$prenom, tel=$tel, mail=$mail");
    header('Location:../utilisateur.php');
    die();
}


// suppression utilisateur
if(isset($_GET['userDelete'])){
    $id = $db->quote($_GET['userDelete']);
    $db->query("DELETE FROM 2user WHERE user_id=$id");
    header('Location:utilisateur.php');
    die();
}



//Affichage des réservations
$reservationv1 = $db->query('SELECT * FROM 2reservation');
$reservations = $reservationv1->fetchAll();

//Activation réservation
if(isset($_GET['resActivation'])){
    $id = $db->quote($_GET['resActivation']);
    $db->query("UPDATE 2reservation SET indisponible = '0'  WHERE reservation_id=$id");
    header('Location:Reservation.php');
    die();
}

//Désactivation réservation
if(isset($_GET['resDesactivation'])){
    $id = $db->quote($_GET['resDesactivation']);
    $db->query("UPDATE 2reservation SET indisponible = '1'  WHERE reservation_id=$id");
    header('Location:Reservation.php');
    die();
}

//Ajouter une réservation
if (isset($_POST['debutAdd']) && isset($_POST['finAdd']) && isset($_POST['vehiculeAdd']) && isset($_POST['userAdd'])){
    $demande=$db->quote(date("Y-m-d"));
    $debut=$db->quote($_POST['debutAdd']);
    $fin=$db->quote($_POST['finAdd']);
    $vehicule=$db->quote($_POST['vehiculeAdd']);
    $user=$db->quote($_POST['userAdd']);
    $db->query("INSERT INTO `2reservation` SET date_reservation=$demande, date_debut=$debut, date_fin=$fin, vehicule_id=$vehicule, user_id=$user");
    header('Location:../reservation.php');
    die();
}

//Modifier d'une réservation
if (isset($_POST['debut']) && isset($_POST['fin']) && isset($_POST['vehicule']) && isset($_POST['user'])){
    $id = $db->quote($_GET['resEdit']);
    $debut=$db->quote($_POST['debut']);
    $fin=$db->quote($_POST['fin']);
    $vehicule=$db->quote($_POST['vehicule']);
    $user=$db->quote($_POST['user']);
    $db->query("UPDATE `2reservation` SET date_debut=$debut, date_fin=$fin, vehicule_id=$vehicule, user_id=$user WHERE reservation_id=$id ");
    header('Location:../reservation.php');
    die();
}




// suppression réservation
if(isset($_GET['resDelete'])){
    $id = $db->quote($_GET['resDelete']);
    $db->query("DELETE FROM 2reservation WHERE reservation_id=$id");
    header('Location:Reservation.php');
    die();
}



?>

