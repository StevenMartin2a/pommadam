-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 04 juin 2019 à 16:48
-- Version du serveur :  10.1.33-MariaDB
-- Version de PHP :  7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `pommadan`
--

-- --------------------------------------------------------

--
-- Structure de la table `2reservation`
--

CREATE TABLE `2reservation` (
  `reservation_id` int(11) NOT NULL,
  `date_reservation` date NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `vehicule_id` int(11) NOT NULL,
  `indisponible` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `2reservation`
--

INSERT INTO `2reservation` (`reservation_id`, `date_reservation`, `date_debut`, `date_fin`, `user_id`, `vehicule_id`, `indisponible`) VALUES
(1, '2019-04-28', '2019-05-01', '2019-05-03', 1, 1, 0),
(2, '2019-05-01', '2019-05-05', '2019-05-11', 2, 6, 0),
(3, '2019-05-01', '2019-05-12', '2019-05-17', 1, 8, 0),
(10, '2019-05-29', '2019-05-30', '2019-06-01', 2, 2, 0);

-- --------------------------------------------------------

--
-- Structure de la table `2user`
--

CREATE TABLE `2user` (
  `user_id` int(11) NOT NULL,
  `nom` varchar(250) NOT NULL,
  `prenom` varchar(250) NOT NULL,
  `tel` int(11) NOT NULL,
  `mail` varchar(250) NOT NULL,
  `indisponible` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `2user`
--

INSERT INTO `2user` (`user_id`, `nom`, `prenom`, `tel`, `mail`, `indisponible`) VALUES
(1, 'Smith', 'Hannah', 659456233, 'hannah@gmail.com', 0),
(2, 'Lespez', 'Tommy', 657481285, 'Tommy@gmail.com', 0),
(3, 'Pachat', 'toto', 265489123, 'toto@miauou.com', 0),
(5, 'fizef', 'prenom', 231930670, 'efef', 0);

-- --------------------------------------------------------

--
-- Structure de la table `2vehicule`
--

CREATE TABLE `2vehicule` (
  `vehicule_id` int(11) NOT NULL,
  `modele` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `nb_place` int(11) NOT NULL,
  `kilometrage` int(11) NOT NULL,
  `etat` varchar(250) NOT NULL,
  `indisponible` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `2vehicule`
--

INSERT INTO `2vehicule` (`vehicule_id`, `modele`, `type`, `nb_place`, `kilometrage`, `etat`, `indisponible`) VALUES
(1, 'debub', 'Voiture', 4, 69000, 'En rÃ©vision', 0),
(2, 'debub', 'Voiture', 4, 69000, 'En rÃ©vision', 0),
(3, 'debub', 'Voiture', 4, 69000, 'En rÃ©vision', 0),
(4, 'debub', 'Voiture', 4, 69000, 'En rÃ©vision', 0),
(5, 'debub', 'Voiture', 4, 69000, 'En rÃ©vision', 0),
(6, 'debub', 'Camionnette', 4, 69000, 'En rÃ©vision', 0),
(7, 'debub', 'Camionnette', 4, 69000, 'En rÃ©vision', 0),
(8, 'debub', 'Camion', 4, 69000, 'En rÃ©vision', 0),
(9, 'debub', 'Camion', 4, 69000, 'En rÃ©vision', 0),
(12, 'twingo', '', 5, 69000, 'En rÃ©vision', 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `2reservation`
--
ALTER TABLE `2reservation`
  ADD PRIMARY KEY (`reservation_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `vehicule_id` (`vehicule_id`);

--
-- Index pour la table `2user`
--
ALTER TABLE `2user`
  ADD PRIMARY KEY (`user_id`);

--
-- Index pour la table `2vehicule`
--
ALTER TABLE `2vehicule`
  ADD PRIMARY KEY (`vehicule_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `2reservation`
--
ALTER TABLE `2reservation`
  MODIFY `reservation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `2user`
--
ALTER TABLE `2user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `2vehicule`
--
ALTER TABLE `2vehicule`
  MODIFY `vehicule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `2reservation`
--
ALTER TABLE `2reservation`
  ADD CONSTRAINT `2reservation_ibfk_1` FOREIGN KEY (`vehicule_id`) REFERENCES `2vehicule` (`vehicule_id`),
  ADD CONSTRAINT `2reservation_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `2user` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
