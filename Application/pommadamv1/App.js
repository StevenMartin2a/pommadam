import React, { Component } from 'react';
import { AppRegistry, View, Button, Text, StyleSheet, ScrollView } from 'react-native';


const data = require('./data/data.json');


export default class AwesomeProject extends Component {
  render() {
    return (

<View>
  <View>
    <Text h1 style={styles.header}>Pommadam</Text>
  </View>
  

        <ScrollView>
        <View style={styles.card}>
          <Text>Modèle: {v1.modele}</Text>
          <Text>Nombre de place: {v1.place}</Text>
          <Text>Kilométrage: {v1.kilometrage}</Text>
          <Text>Etat: {v1.etat}</Text>
          <View style={styles.button}>    
            <Button
            title="Réserver"
          />
          </View>

        </View>
        <View style={styles.card}>
          <Text>Modèle: {v2.modele}</Text>
          <Text>Nombre de place: {v2.place}</Text>
          <Text>Kilométrage: {v2.kilometrage}</Text>
          <Text>Etat: {v2.etat}</Text>
          <View style={styles.button}>    
            <Button
            title="Réserver"
          />
          </View>

        </View>
        <View style={styles.card}>
          <Text>Modèle: {v3.modele}</Text>
          <Text>Nombre de place: {v3.place}</Text>
          <Text>Kilométrage: {v3.kilometrage}</Text>
          <Text>Etat: {v3.etat}</Text>
          <View style={styles.button}>    
            <Button
            title="Réserver"
          />
          </View>

        </View>
        <View style={styles.card}>
          <Text>Modèle: {v4.modele}</Text>
          <Text>Nombre de place: {v4.place}</Text>
          <Text>Kilométrage: {v4.kilometrage}</Text>
          <Text>Etat: {v4.etat}</Text>
          <View style={styles.button}>    
            <Button
            title="Réserver"
          />
          </View>

        </View>
        <View style={styles.card}>
          <Text>Modèle: {v5.modele}</Text>
          <Text>Nombre de place: {v5.place}</Text>
          <Text>Kilométrage: {v5.kilometrage}</Text>
          <Text>Etat: {v5.etat}</Text>
          <View style={styles.button}>    
            <Button
            title="Réserver"
          />
          </View>
        </View>
        <View style={styles.card}>
          <Text>Modèle: {v6.modele}</Text>
          <Text>Nombre de place: {v6.place}</Text>
          <Text>Kilométrage: {v6.kilometrage}</Text>
          <Text>Etat: {v6.etat}</Text>
          <View style={styles.button}>    
            <Button
            title="Réserver"
          />
          </View>

        </View>
        <View style={styles.card}>
          <Text>Modèle: {v7.modele}</Text>
          <Text>Nombre de place: {v7.place}</Text>
          <Text>Kilométrage: {v7.kilometrage}</Text>
          <Text>Etat: {v7.etat}</Text>
          <View style={styles.button}>    
            <Button
            title="Réserver"
          />
          </View>

        </View>
        <View style={styles.card}>
          <Text>Modèle: {v8.modele}</Text>
          <Text>Nombre de place: {v8.place}</Text>
          <Text>Kilométrage: {v8.kilometrage}</Text>
          <Text>Etat: {v8.etat}</Text>
          <View style={styles.button}>    
            <Button
            title="Réserver"
          />
          </View>

        </View>
        <View style={styles.card}>
          <Text>Modèle: {v9.modele}</Text>
          <Text>Nombre de place: {v9.place}</Text>
          <Text>Kilométrage: {v9.kilometrage}</Text>
          <Text>Etat: {v9.etat}</Text>
          <View style={styles.button}>    
            <Button
            title="Réserver"
          />
          </View>

        </View>
        <View style={styles.card}>
          <Text>Modèle: {v10.modele}</Text>
          <Text>Nombre de place: {v10.place}</Text>
          <Text>Kilométrage: {v10.kilometrage}</Text>
          <Text>Etat: {v10.etat}</Text>
          <View style={styles.button}>    
            <Button
            title="Réserver"
          />
          </View>
        </View>
        <View>
    <Text style={styles.header}></Text>
  </View>
        </ScrollView>

</View>

      
    );
  }
}

AppRegistry.registerComponent('AwesomeProject', () => AwesomeProject);

//data véhicule
const v1 = data.vehicle1
const v2 = data.vehicle2
const v3 = data.vehicle3
const v4 = data.vehicle4
const v5 = data.vehicle5
const v6 = data.vehicle6
const v7 = data.vehicle7
const v8 = data.vehicle8
const v9 = data.vehicle9
const v10 = data.vehicle10

//Style
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',

  },
  header: {
    fontSize: 30,
    textAlign: 'center',
    margin: 10,
    color: '#34C924',
  },
  instructions: {
    flex : 2,
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  card: {
    borderColor: '#000000',
    borderBottomWidth: 1,
    fontSize: 10,
    margin:5,
  },
  button: {
    marginBottom:10,
  }
});